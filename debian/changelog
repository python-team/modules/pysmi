pysmi (0.3.4-3) unstable; urgency=medium

  * Team upload.
  * Add a patch to handle the removal of "pyasn1.compat.octets"
    from python3-pyasn1 (Closes: #1098606, #1098592).

    Thank you Santiago Vila for rechecking.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 23 Feb 2025 23:15:55 +0100

pysmi (0.3.4-2) unstable; urgency=medium

  * Team Upload
  * Set Team as Maintainer per new DPT Policy
  * Use dh-sequence-python3
  * Set "Rules-Requires-Root: no"
  * Compatibility with Sphinx 8 (Closes: #1090123)
  * Fix SyntaxError (Closes: #1071281)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 25 Dec 2024 20:23:00 +0100

pysmi (0.3.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.3.4.

  [ Debian Janitor ]
  * Update lintian override info to new format on line 2.
  * Bump debhelper from old 10 to 13.
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Apply multi-arch hints. + python-pysmi-doc: Add Multi-Arch: foreign.

 -- Bastian Germann <bage@debian.org>  Thu, 21 Dec 2023 10:51:26 +0000

pysmi (0.3.2-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 21:41:15 -0400

pysmi (0.3.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #937539

 -- Sandro Tosi <morph@debian.org>  Fri, 20 Mar 2020 21:54:47 -0400

pysmi (0.3.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Vincent Bernat ]
  * New upstream release
    - Fix FTBFS about test MIB not found. Closes: #911692.
  * d/rules: don't install mibcopy.py.

 -- Vincent Bernat <bernat@debian.org>  Tue, 23 Oct 2018 21:10:20 +0200

pysmi (0.2.2-1) unstable; urgency=low

  * Initial release (closes: #885863)

 -- Vincent Bernat <bernat@debian.org>  Sat, 30 Dec 2017 15:25:10 +0000
